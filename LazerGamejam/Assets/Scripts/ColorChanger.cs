﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChanger : MonoBehaviour {

    public Color newColor;
    private Laser laser;
    public MeshRenderer lens;

    private void Awake()
    {
        laser = GetComponent<Laser>();
        lens.material.color = newColor;
    }

    public void ChangeColor()
    {
        laser.DrawLaser(transform.position, transform.forward, newColor);
    }
}
