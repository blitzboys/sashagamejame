﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    LineRenderer lr;
    private int linePoints = 1;
    private Color offC = new Color(0,0,0,0);
    private int timer;

    private void Start()
    {
        lr = GetComponent<LineRenderer>();

    }
    private void Update()
    {
        linePoints = 1;
        lr.SetPosition(0, transform.position);
        if (timer == 3)
        {
            lr.startColor = offC;
            lr.endColor = offC;
            timer = 0;
        }
        timer++;
    }

    public void DrawLaser(Vector3 pos, Vector3 dir, Color c)
    {
        timer = 0;
        RaycastHit hit;
        Physics.Raycast(pos, dir, out hit);
        Debug.DrawRay(pos, dir, Color.red);

        linePoints++;
        lr.positionCount = linePoints;
        lr.SetPosition(linePoints - 1, hit.point);
        lr.startColor = c;
        lr.endColor = c;

        if (hit.collider && !(linePoints > 10))
        {
            GameObject hitObject = hit.collider.gameObject;
            switch (hitObject.tag)
            {
                case "Mirror":
                    Vector3 reflectDirection = Vector3.Reflect(dir, hit.normal);
                    DrawLaser(hit.point, reflectDirection, c);
                    break;

                case "Lens":
                    hitObject.GetComponent<LensScript>().LensLaser(c);
                    break;

                case "ColorLens":
                    hitObject.GetComponent<ColorChanger>().ChangeColor();
                    break;

                case "Finish":
                    hitObject.GetComponent<EndPoint>().LaserEnd(c);
                    break;

                case "Splitter":
                    hitObject.GetComponent<Splitterscript>().SplitLaser(c);
                    break;

                case "Relay":
                    hitObject.GetComponent<LaserRelay>().RealayIn(c);
                    break;

                case "Combiner":
                    hitObject.GetComponent<CombineScript>().SetLaser(c);
                    break;
            }
        }
    }
}
