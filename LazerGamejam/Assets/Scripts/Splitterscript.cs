﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Splitterscript : MonoBehaviour
{
    public GameObject[] laserPoint;
    private MeshRenderer mr;
    private int timer;

    void Start ()
    {
        mr = GetComponentInChildren<MeshRenderer>();
    }
	

	void Update ()
    {
        if (timer == 3)
        {
            mr.material.color = Color.white;
        }
        timer++;
    }

    public void SplitLaser(Color c)
    {
        timer = 0;
        mr.material.color = c;
        for (int i = 0; i < laserPoint.Length; i++)
        {
            laserPoint[i].GetComponent<Laser>().DrawLaser(laserPoint[i].transform.position, laserPoint[i].transform.right, c);
        }
    }
}
