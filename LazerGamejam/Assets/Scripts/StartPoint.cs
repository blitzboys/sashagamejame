﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartPoint : MonoBehaviour {
    public Color laserColor;
    public EndPoint end;
    private Laser laser;
    public bool firing;
    private MeshRenderer mr;
    private Light l;

    void Start () {
        laser = GetComponent<Laser>();
        mr = GetComponentInChildren<MeshRenderer>();
        l = GetComponentInChildren<Light>();

        l.color = laserColor;

    }

    // Update is called once per frame
    void Update()
    {
        if (firing)
        {
            laser.DrawLaser(transform.position, transform.forward, laserColor);
            mr.material.color = laserColor;
            l.enabled = true;

        }
        else
        {
            mr.material.color = Color.white;
            l.enabled = false;
        }
    }

    public IEnumerator LaserTimer()
    {
        firing = true;
        float timer = 0;

        while(timer < 30)
        {
            timer++;
            Debug.Log(timer);
            yield return null;
        }
        firing = false;
    }
}
