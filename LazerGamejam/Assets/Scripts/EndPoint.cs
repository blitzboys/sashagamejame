﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndPoint : MonoBehaviour {
    public Color endColor;
    public Animator[] DoorAnim;
    private int timer;
    public MeshRenderer correctcolor;
    public MeshRenderer colorchecker;
    MeshRenderer ball;
    public AudioClip anouncement;
    AudioSource player;
    public bool isplaying;
    bool playedOnce;
    public bool isFinal;

    public void Start()
    {
        correctcolor.material.color = endColor;
        ball = GetComponent<MeshRenderer>();
        player = GetComponent<AudioSource>();
    }

    public void Update()
    {
        if(timer == 3)
        {
            colorchecker.material.color = Color.white;
            ball.material.color = Color.white;
        }
        timer++;
        if(isplaying && !playedOnce)
        {
            player.clip = anouncement;
            player.Play();
            playedOnce = true;
        }
    }

    public void LaserEnd(Color c)
    {
        ball.material.color = c;
        timer = 0;
        if(c.r == endColor.r && c.g == endColor.g && c.b == endColor.b)
        {
            colorchecker.material.color = Color.green;
            if (isFinal)
            {
                UIFadeInScript outro = GameObject.Find("IntroManager").GetComponent<UIFadeInScript>();
                StartCoroutine(outro.FadeOut());
            }
            for (int i = 0; i < DoorAnim.Length; i++)
            {
                DoorAnim[i].Play("DoorOpenState");
                isplaying = true;
            }
        }
        else
        {
            colorchecker.material.color = Color.red;
        }
        
    }

}
