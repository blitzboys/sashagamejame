﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserRelay : MonoBehaviour {
    public LaserRelay[] outLasers;
    public bool isInput;
    private Laser laser;
    public  MeshRenderer mr;
    private int timer;

    private void Start()
    {
        if (!isInput)
        {
            laser = GetComponent<Laser>();
        }
        else
        {
            mr.GetComponent<MeshRenderer>();
        }
    }

    private void Update()
    {
        if (isInput && timer == 3)
        {
            mr.material.color = Color.white;
            timer = 0;
        }
        timer++;
    }

    public void RealayIn(Color c)
    {
        for(int i = 0; i < outLasers.Length; i++)
        {
            outLasers[i].RelayOut(c);
        }
        mr.material.color = c;
        timer = 0;
    }
    
    public void RelayOut(Color c)
    {
        laser.DrawLaser(transform.position, transform.forward, c);
    }

}
