﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LensScript : MonoBehaviour
{
    private Laser laser;
    private MeshRenderer ball;
    private int timer;

    void Start ()
    {
        laser = GetComponent<Laser>();
        ball = GetComponentInChildren<MeshRenderer>();

	}

    private void Update()
    {
        if (timer == 3)
        {
            ball.material.color = Color.white;
        }
        timer++;
    }

    public void LensLaser(Color c)
    {
        timer = 0;
        ball.material.color = c;
        laser.DrawLaser(transform.position, transform.right, c);
    }
}
