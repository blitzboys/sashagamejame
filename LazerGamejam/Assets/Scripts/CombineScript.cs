﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombineScript : MonoBehaviour
{
    public bool isReciever;
    public bool isRecieving;
    private Laser laser;
    public CombineScript[] recievers;
    MeshRenderer meshrenderer;
    Color currentColor;
    private int timer;


    void Start()
    {
        if (!isReciever)
        {
            laser = GetComponent<Laser>();
        }
        else
        {
            meshrenderer = GetComponent<MeshRenderer>();
        }
    }

    void Update()
    {

        if (!isReciever)
        {
            if (recievers[0].isRecieving && recievers[1].isRecieving)
            {
                CombinerLaser(recievers[0].currentColor, recievers[1].currentColor);
            }
        }

        if (timer == 3)
        {
            isRecieving = false;
            if (meshrenderer != null)
            {
                meshrenderer.material.color = Color.white;
            }
        }
        timer++;
    }

    public void CombinerLaser(Color a, Color b)
    {
        Color c = a + b;
        if(c.r > 1)
        {
            c.r = 1;
        }
        laser.DrawLaser(transform.position, transform.right, c);
    }

    public void SetLaser(Color c)
    {
        timer = 0;
        isRecieving = true;
        meshrenderer.material.color = c;
        currentColor = c;
    }
}
