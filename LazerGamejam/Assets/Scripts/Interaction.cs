﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaction : MonoBehaviour
{
    public float distance;
    public bool holdingItem;
    public GameObject grabbedObj;
    public Rigidbody ObjRB;
    public bool objectIsKinematic;
    public GameObject fpsController;
    public Camera mainCamera;
    bool sandvichPlayed;

	void Start ()
    {
        fpsController = GameObject.Find("FPSController");
        mainCamera = GetComponent<Camera>();
        sandvichPlayed = false;
	}
	
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (holdingItem)
            {
                holdingItem         = false;
                ObjRB.useGravity    = true;
                if (objectIsKinematic)
                {
                    ObjRB.isKinematic = true;
                }
            }
            else
            {
                InteractionRay();
            }
        }

        GrabbedObjPos(grabbedObj);
	}

    void InteractionRay()
    {
        int x       = Screen.width / 2;
        int y       = Screen.height / 2;
        Ray ray     = mainCamera.ScreenPointToRay(new Vector3(x, y));
        RaycastHit hit;
        Debug.DrawRay(ray.origin, ray.direction * distance, Color.red);
        if (Physics.Raycast(ray, out hit, distance))
        {
            if (hit.collider.gameObject.GetComponent<Rigidbody>() != null)
            {
                grabbedObj              = hit.collider.gameObject;
                ObjRB                   = grabbedObj.GetComponent<Rigidbody>();
                ObjRB.useGravity        = false;
                holdingItem             = true;
                if (ObjRB.isKinematic)
                {
                    objectIsKinematic   = true;
                    ObjRB.isKinematic   = false;
                }
                else
                {
                    objectIsKinematic   = false;
                }
            }
            else
            {
                objectIsKinematic       = false;
                holdingItem             = false;
                grabbedObj              = null;
            }
            /*if (hit.collider.gameObject.CompareTag("Button")){
                Debug.Log("shoot laser");
                StartPoint startPoint = GameObject.Find("LaserManager").GetComponent<StartPoint>();
                if (!startPoint.firing)
                {
                    startPoint.StartCoroutine(startPoint.LaserTimer());
                }
            }*/
            if (hit.collider.gameObject.CompareTag("Sandvich") && !sandvichPlayed)
            {
                AudioSource sandvich = GameObject.FindGameObjectWithTag("Sandvich").GetComponent<AudioSource>();
                sandvich.Play();
                sandvichPlayed = true;
            }
        }
    }

    void GrabbedObjPos(GameObject obj)
    {
        if(obj != null && holdingItem)
        {
            Rigidbody rb = obj.GetComponent<Rigidbody>();
            rb.MovePosition(Vector3.Lerp(obj.transform.position, transform.position + transform.forward * distance, Time.deltaTime * 4));
            //obj.transform.position = Vector3.Lerp(obj.transform.position, transform.position + transform.forward * distance, Time.deltaTime * 4);
            //obj.transform.rotation = new Quaternion(0,fpsController.transform.rotation.y,0,0);
            var d = Input.GetAxis("Mouse ScrollWheel");
            if (d > 0f)
            {
                // scroll up
                obj.transform.Rotate(Vector3.up, 1.5f);
            }
            else if (d < 0f)
            {
                // scroll down
                obj.transform.Rotate(Vector3.up, -1.5f);
            }

        }
    }
}
