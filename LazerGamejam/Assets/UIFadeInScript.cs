﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFadeInScript : MonoBehaviour
{
    Image fadeIn;
    float alpha;
    public AudioClip intro;
    AudioSource clipPlayer;

	void Start ()
    {
        alpha = 1f;
        fadeIn = GetComponent<Image>();
        clipPlayer = GetComponent<AudioSource>();
        fadeIn.color = Color.black;
        StartCoroutine(FadeIn());
	}
	
	void Update ()
    {
        if (!clipPlayer.isPlaying)
        {

        }
	}

    IEnumerator FadeIn()
    {
        clipPlayer.clip = intro;
        clipPlayer.Play();
        while (fadeIn.color.a > 0)
        {
            fadeIn.color = new Color(fadeIn.color.r, fadeIn.color.g, fadeIn.color.b, alpha -= 0.005f);
            yield return null;
        }
    }

    public IEnumerator FadeOut()
    {
        yield return new WaitForSeconds(50f);
        while (fadeIn.color.a < 1)
        {
            fadeIn.color = new Color(fadeIn.color.r, fadeIn.color.g, fadeIn.color.b, alpha += 0.00001f);
            yield return null;
        }
    }
}
